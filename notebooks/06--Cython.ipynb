{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Cython\n",
    "**Python for HPC course**\n",
    "\n",
    "\n",
    "Max Planck Computing and Data Facility, Garching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Why Cython?\n",
    "\n",
    "### Goal\n",
    "\n",
    "* Make implementation of C/C++ extensions for Python easy\n",
    "    * Existing external C/C++ libraries\n",
    "    * New Cython or C/C++ implementations, typically of code hotspots\n",
    "\n",
    "### Documentation\n",
    "\n",
    "* http://cython.org/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Cython?\n",
    "\n",
    "* Cython is Python extended with C data types  \n",
    "  $\\to$ Cython is a superset of the Python language\n",
    "* Cython is a source-to-source compiler\n",
    "\n",
    "### Workflow\n",
    "\n",
    "1. create Cython source file (`.pyx`), e.g. by moving performance-critical code from Python to Cython\n",
    "2. apply Cython compiler which translates `.pyx` code into `.c` code\n",
    "3. C code is finally compiled into a Python module (Linux `.so`) by a C compiler (e.g. `gcc`)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Advantages\n",
    "\n",
    "* achieve performance close to native C/C++ or Fortran code while keeping Python-like code\n",
    "    * Cython code is compiled, not interpreted\n",
    "    * compiler optimizations can be applied (e.g. vectorization)\n",
    "    * OpenMP thread parallelization becomes possible\n",
    "* Cython integrates well with NumPy arrays\n",
    "* Cython provides an easy way to bind external C libraries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Compilation\n",
    "\n",
    "* in principle, shell scripts or Makefiles can be used (but avoid this)\n",
    "* better: use a simple `setup.py` script to compile and install your Cython code properly\n",
    "* see the simple example at `cython/hello_world`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "```python\n",
    "# hello_world.pyx\n",
    "def say_hello():\n",
    "    print(\"Hello World!\")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "```python\n",
    "# setup.py\n",
    "from setuptools import setup\n",
    "from Cython.Build import cythonize\n",
    "setup(name = \"hello_world\",\n",
    "      ext_modules = cythonize(\"hello_world.pyx\"))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```bash\n",
    "# explicit compilation of the cython extension\n",
    "$ python setup.py build_ext --inplace\n",
    "# or compilation from the build-backend during installation\n",
    "$ pip install .\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "```bash\n",
    "$ python -c \"import hello_world; hello_world.say_hello()\"\n",
    "Hello World!\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Cython integration with Jupyter notebooks\n",
    "\n",
    "* Cython code can be compiled and used directly from a Jupyter notebook after loading (`%load_ext Cython`)\n",
    "* use the cell magic `%%cython` to compile a Jupyter cell with Cython code\n",
    "* using `-c=` allows to specify compiler optimization flags, for linker flags use `--link-args`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "%load_ext Cython"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### A first simple example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "def f(n):\n",
    "    \"\"\"Sum the first (n-1) integers.\"\"\"\n",
    "    a = 0\n",
    "    for i in range(n):\n",
    "        a += i\n",
    "    return a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "%%cython -c=-O3 -c=-march=native\n",
    "\n",
    "def g(unsigned long long int n):\n",
    "    \"\"\"Sum the first (n-1) integers in Cython.\"\"\"\n",
    "    cdef unsigned long long int a = 0\n",
    "    cdef unsigned long long int i\n",
    "    for i in range(n):\n",
    "        a += i\n",
    "    return a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "n = 2**24"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "743 ms ± 11.8 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)\n"
     ]
    }
   ],
   "source": [
    "%timeit f(n)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2.53 ms ± 54.3 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit g(n)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "33.6 ms ± 1.22 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)\n"
     ]
    }
   ],
   "source": [
    "# comparison to numpy\n",
    "import numpy as np\n",
    "def h(n):\n",
    "    return np.sum(np.arange(n))\n",
    "\n",
    "%timeit h(n)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "140737479966720 140737479966720 140737479966720 140737479966720\n"
     ]
    }
   ],
   "source": [
    "def analytic(n):\n",
    "    return n * (n-1) // 2\n",
    "\n",
    "print(f(n), g(n), h(n), analytic(n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Cython tutorial\n",
    "* step-by-step tutorial, going from Python-code to efficient Cython-code\n",
    "\n",
    "### $\\to$ continue with the `Diffusion.ipynb` notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Interfacing C/C++ code with Cython\n",
    "\n",
    "### $\\rightarrow$ continue with the `Interfacing_with_C_and_F.ipynb` notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Cython summary\n",
    "\n",
    "* Cython speeds up Python code by converting it into C and compiling it\n",
    "* Workflow\n",
    "    * start with existing (critical) Python code, move it to `.pyx` file, create basic `setup.py`\n",
    "    * introduce basic type declarations, e.g. `cdef int a`\n",
    "    * introduce NumPy array declarations, e.g.  \n",
    "      `np.ndarray[np.float64_t, ndim=2] grid`\n",
    "    * use the '--annotate' flag to get hints at performance-critical Python remnants\n",
    "    * finally, test and relax array checking via decorators\n",
    "* Further reading\n",
    "    * http://cython.org/ for in-depth information, in particular\n",
    "    * http://cython.readthedocs.io/en/latest/src/tutorial/numpy.html"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
