{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Introduction\n",
    "\n",
    "**Python for HPC course**\n",
    "\n",
    "Sebastian Kehl, Sebastian Ohlmann, Klaus Reuter\n",
    "\n",
    "_26-28 November 2024_\n",
    "\n",
    "HPC Application Support Group\n",
    "\n",
    "Max Planck Computing and Data Facility, Garching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Why Python in HPC?\n",
    "* Efficiency with respect to development effort:  \n",
    "  $\\rightarrow$ Python helps you to get \"things done\" quickly and focus on your science.\n",
    "* Efficiency with respect to hardware utilization:  \n",
    "  $\\rightarrow$ Python-based codes can perform well if implemented properly.\n",
    "* Using Python may achieve a good overall efficiency for small- to medium-scale codes\n",
    "* And finally ... Python is already there, so we have do deal with it!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Examples for \"real\" HPC Simulation Codes using Python\n",
    "\n",
    "### [GPAW](https://wiki.fysik.dtu.dk/gpaw/)\n",
    "\n",
    "* Density-functional theory code for material science\n",
    "* Implemented as a Python module (C core, Python UI)\n",
    "* Parallelization: MPI + OpenMP, scales to O(10k) cores\n",
    "\n",
    "### [ESPResSo++](http://www.espresso-pp.de)\n",
    "\n",
    "* Molecular dynamics package for soft matter simulation,  \n",
    "  MPI for Polymer Research, Mainz\n",
    "* Implemented as a Python module (C++ core, Python UI)\n",
    "* Parallelization: Domain decomposition using MPI"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "##  Use cases often relevant to our audience\n",
    "\n",
    "* prototype implementations of numerical or AI code that should run (much) faster when moving towards production\n",
    "* data analysis scripts that should run faster and/or operate in parallel\n",
    "* IO-handling and processing of large numerical data\n",
    "\n",
    "$\\to$ very often, efficient hardware utilization and good speedups can be gained without too much effort by applying the right techniques and tricks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Scope of this course\n",
    "* Learn about the tools and techniques to write efficient Python code for scientific computing\n",
    "![gain vs effort](fig/gain_vs_effort.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Course Outline\n",
    "\n",
    "* Introduction\n",
    "* Quick refresher of Python and the Python Ecosystem\n",
    "* Basics of High Performance Computing\n",
    "* Scientific Computing with Python: NumPy and SciPy\n",
    "* Performance: Cython, JIT (Numba, JAX), C/Fortran interfacing\n",
    "* Parallelism: multithreading, multiprocessing, GPU computing, Dask, mpi4py\n",
    "* Software Engineering, Packaging, Profiling, Testing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## *Python for HPC* complements advanced HPC courses\n",
    "\n",
    "* We cannot cover traditional HPC topics in depth, and would highly recommend the following courses\n",
    "    * Node-level performance engineering\n",
    "    * Parallel programming with MPI\n",
    "    * Parallel programming with OpenMP\n",
    "    * Advanced parallel programming with OpenMP and MPI\n",
    "* Watch out for these courses which are regularly offered by HLRS, LRZ, RRZE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## About this Python for HPC course\n",
    "\n",
    "* Practical hands-on approach with code examples\n",
    "* Presentation is based on Jupyter notebooks\n",
    "* Exercises based on Jupyter notebooks complement the presentations\n",
    "* Course material available for download at  \n",
    "  https://gitlab.mpcdf.mpg.de/mpcdf/python-for-hpc-exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Software prerequisites for the Jupyter notebooks and exercises\n",
    "* Python >=3.6\n",
    "* JupyterLab\n",
    "* NumPy, SciPy\n",
    "* matplotlib\n",
    "* Cython\n",
    "* gcc, gfortran\n",
    "* Numba\n",
    "* (CuPy, JAX)\n",
    "* (Dask, Dask-MPI, MPI, mpi4py)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Software Option 1: Cloud-based MPCDF Jupyter notebooks\n",
    "\n",
    "* Use the link communicated by email to access a Jupyter service in the MPCDF HPC cloud\n",
    "* The course material *and* software are provided via an interactive JupyterLab interface\n",
    "* Each instance provides access to several virtual CPU cores and a few GB of RAM\n",
    "* Please keep the following points in mind\n",
    "    * Use the JupyterLab menu **File $\\to$ Shut down** to free resources when finished\n",
    "    * A session is terminated after 8 hours, or after 2 hours of inactivity\n",
    "    * The storage is only temporary, i.e. download your modified notebooks and data in time via the GUI"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Software Option 2: Python infrastructure on the MPCDF HPC systems\n",
    "* Python is provided via conda-based Python distributions (now based on 'conda-forge', historically Anaconda)\n",
    "* software is accessible via environment modules, e.g.  \n",
    "```bash\n",
    "$ module purge\n",
    "$ module load gcc/12 impi/2021.9\n",
    "$ module load anaconda/3/2023.03\n",
    "$ module load mpi4py/3.1.4\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Software Option 3: Python environment for your local computer\n",
    "\n",
    "* On a recent Linux system, install all the necessary packages via the package manager and `pip`\n",
    "* Alternatively, install [Miniforge](https://conda-forge.org/miniforge/) and use the `conda` package manager and the file `environment.yml` that is provided together with the course material to add all necessary packages.  \n",
    "```bash\n",
    "$ conda env create --file environment.yml\n",
    "$ conda activate pyhpc\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
