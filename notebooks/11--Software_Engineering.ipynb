{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Software Engineering\n",
    "**Python for HPC course**\n",
    "\n",
    "\n",
    "Max Planck Computing and Data Facility, Garching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Software Engineering with Python\n",
    "A whirlwind tour through\n",
    "* Packaging\n",
    "* Dependency management\n",
    "* Documentation\n",
    "* Version Control\n",
    "* Software Testing\n",
    "* Distribution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Further reading: Software carpentry\n",
    "* Project for teaching computing skills and good practices for researchers\n",
    "* https://software-carpentry.org/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Software Packaging\n",
    "* The Python ecosytem is inherently geared toward software packaging\n",
    "    * great for \"professional\" software distribution (e.g., via PyPI)\n",
    "    * create source (source code archives) or binary distributions (wheels)\n",
    "    * metadata specification (version, authorship, license, **dependencies**, ...)\n",
    "    * Compilation of Cython, C/C++, Fortran extensions (Makefile replacement)\n",
    "    * Installation to system, per-user, or arbitrary locations\n",
    "    * See examples in the folder `setuptools`\n",
    "    * https://packaging.python.org/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Package configuration:\n",
    "\n",
    "* the python community converged to define package specification in `pyproject.toml` (PEP518, PEP517)\n",
    "* packaging is split into\n",
    "    * *frontend*, e.g.: `pip`, `wheel`, `build`, ...\n",
    "    * *backend*, e.g.,  `setuptools` (the historical reference), `scikit-build-core`, `flit`, `hatch`, `pdm`, `poetry`, ...\n",
    "\n",
    "\n",
    "* build system isolation (PEP518)\n",
    "* core metadata specification in `pyproject.toml` (PEP621), different backends then may use more/different files for the build configuration on top"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Example: `pyproject.toml` with setuptools (see folder `setuptools`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "```toml\n",
    "[build-system]\n",
    "requires = [\"setuptools\"]\n",
    "build-backend = \"setuptools.build_meta\"\n",
    "\n",
    "[project]\n",
    "name = \"helloworld\"\n",
    "version = \"0.1\"\n",
    "# name of the software package (as it would appear on PyPI)\n",
    "description = \"example package that prints hello world\"\n",
    "# dependencies = [\"numpy\"]\n",
    "authors = [\n",
    "  {name = \"John Doe\", email = \"john.doe@example.mpg.de\"}\n",
    "]\n",
    "license = {text = \"PSF\"}\n",
    "keywords = [\"hello\", \"world\", \"example\"]\n",
    "\n",
    "[project.urls]\n",
    "homepage = \"https://example.mpg.de/helloworld/\"\n",
    "\n",
    "# list of executable(s) that come with the package (if applicable)\n",
    "[project.scripts]\n",
    "hello-world = \"helloworld.cli:hello_world\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Installing and distributing packages\n",
    "* Install package with `pip install .`(implying build of the package)<br>\n",
    "  add `--user` $\\to$ installation path under `~/.local/lib/...`;<br>\n",
    "  this will make the package available for your local user (**not recommended in general**)\n",
    "* Explicitly build package including extensions with `python -m build`\n",
    "* Create distribution packages for sharing\n",
    "    * Source package with `python -m build --sdist`\n",
    "    * Binary package with `python -m build --wheel`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Example folder structure of a simple python package\n",
    "\n",
    "```\n",
    "project_root_directory\n",
    "    pyproject.toml\n",
    "        package_name/                  # folder of the package\n",
    "            __init__.py                # initialize the package\n",
    "            module_a.py                # some python module\n",
    "            module_b.py                # some other module\n",
    "```\n",
    "* can be autodetected with `setuptools`\n",
    "* can be imported with `import package_name` or `from package_name import module_a`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Components of a full-fledged software package\n",
    "\n",
    "* source code\n",
    "* package metadata and specification of extensions (`pyproject.toml` + backend dependend configuration)\n",
    "* user documentation - how to use the package\n",
    "* developer documentation - how to develop code for the package\n",
    "* software tests\n",
    "* commonly expected text files to help people get information quickly (e.g. on public repositories)\n",
    "    * `README`\n",
    "    * `LICENSE`\n",
    "    * `CONTRIBUTING`\n",
    "    * `CHANGELOG`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Further reading:\n",
    "* https://packaging.python.org/\n",
    "* https://setuptools.pypa.io/\n",
    "* https://pip.pypa.io/en/stable/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Dependency management\n",
    "\n",
    "* managing dependencies of multiple python packages in a single location is difficult due to\n",
    "  * conflicts\n",
    "  * maintenance\n",
    "  * reproducability\n",
    "\n",
    "* -> dependency isolation via *virtual environments*\n",
    "\n",
    "\n",
    "* Python provides the built-in `venv` package\n",
    "  * very lightweight\n",
    "  * easy to use\n",
    "  \n",
    "  \n",
    "* Full-fledged package managers provide automated handling of environments\n",
    "  * `pipenv`, `poetry`, `hatch`, `pdm`, ...\n",
    "  * automated handling\n",
    "  * dependency resolution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Example: `venv`\n",
    "\n",
    "* create virtual-environment\n",
    "  ```\n",
    "  > python -m venv myenv\n",
    "  ```\n",
    "  (the option `--system-site-packages` enables access to the base environment)\n",
    "  \n",
    "* activate environment\n",
    "  ```\n",
    "  > source myenv/bin/activate\n",
    "  ```\n",
    "* use as with normal python: `pip install`, `python ...`, ...\n",
    "* deactivate environment\n",
    "  ```\n",
    "  > deactivate\n",
    "  ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Further reading\n",
    "\n",
    "* https://docs.python.org/3/library/venv.html\n",
    "* https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/\n",
    "* the anaconda distribution comes with its own solution for enviroments:\n",
    "  https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/environments.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Documentation\n",
    "### Comments\n",
    "\n",
    "* *Comments* begin with a hash sign and extend until the end of the line\n",
    "    ```python\n",
    "    # TODO initialize list with default values\n",
    "    a = []\n",
    "    ```\n",
    "* *Comments* are programmer's notes, for user documentation use docstrings (see below)\n",
    "* Put (single line) comments where they might help to clarify your code, remind about TODOs, etc. -- but avoid trivial comments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Docstrings\n",
    "\n",
    "* *Docstrings* are used to describe the purpose of a module, function, or class.  They are enclosed in triple quotes and may extend over multiple lines.\n",
    "* Often helpful to understand one's own code after 2 years\n",
    "* Essential when collaborating with others and publishing code\n",
    "* Displayed when using the `help()` builtin of Python\n",
    "* Can be used for automated generation of documentation (e.g. using Sphinx)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class SimulationData:\n",
    "    \"\"\"Class to represent data from hydrodynamics simulations\"\"\"\n",
    "    def __init__(self, path):\n",
    "        \"\"\"Initialize a SimulationData object in a certain path\"\"\"\n",
    "        self.load_data(path)\n",
    "        \n",
    "    def load_data(self, path):\n",
    "        \"\"\"Load the simulation data from given path\"\"\"\n",
    "        # do stuff"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### NumPy-style docstrings\n",
    "\n",
    "* NumPy-style docstrings are widely used in scientific Python programming\n",
    "* Covers datatypes and purpose of input variables and return value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "def add(a, b):\n",
    "    \"\"\"\n",
    "    Add two numbers.\n",
    "    \n",
    "    This functions adds two numbers and returns the result.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    a : int or float\n",
    "        The first summand.\n",
    "    b : int or float\n",
    "        The second summand.\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    int or float\n",
    "        The sum of both the input values a and b.\n",
    "    \"\"\"\n",
    "    return a + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Sphinx Documentation System\n",
    "\n",
    "* Python documentation generator, de-facto standard\n",
    "* Create documentation in HTML, PDF, and other formats from\n",
    "    * reStructuredText (`.rst`) files\n",
    "    * docstrings in source files\n",
    "* Automated generation of documentation is possible via web services such as _GitHub_ and _Read the Docs_\n",
    "\n",
    "http://www.sphinx-doc.org/en/master/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Sphinx\n",
    "\n",
    "Steps\n",
    "\n",
    "* Use `sphinx-quickstart` to create default files for the documentation\n",
    "* Adapt `doc/conf.py` to your needs, enable useful Sphinx addons such as\n",
    "    * `sphinx.ext.autodoc`: include docstrings into documentation\n",
    "    * `sphinx.ext.napoleon`: support for NumPy-style docstrings\n",
    "* Edit `doc/index.rst`, potentially create other rst files\n",
    "* Run `cd doc && make html` to generate the HTML documentation<br>\n",
    "  or run `sphinx-build -b html <source-dir> <out-dir>`\n",
    "\n",
    "Example\n",
    "\n",
    "* See the directory `setuptools/sphinx-doc` for an example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Testing\n",
    "* Tests are a central part of the software development life cycle\n",
    "    * avoid regression\n",
    "    * facilitate collaboration\n",
    "* **Continuous integration**:\n",
    "    * merge contributions often\n",
    "    * run tests automatically for each code commit, e.g. via GitLab-CI\n",
    "* Unit tests\n",
    "    * test functions, classes, modules individually\n",
    "* Integration tests\n",
    "    * test the interplay of units, up to the complete application\n",
    "* Popular python modules: `unittest`, `pytest`, `nose`\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### pytest\n",
    "\n",
    "Here's a minimal example to implement tests for a package \"foobar\":\n",
    "\n",
    "1. Create a file `test_foobar.py` with the content\n",
    "\n",
    "```python\n",
    "import foobar\n",
    "\n",
    "def test_foobar_functionality()\n",
    "    assert(foobar.whatever() == True)\n",
    "```\n",
    "\n",
    "2. Run the command `pytest`\n",
    "\n",
    "Find full documentation at\n",
    "https://docs.pytest.org/en/latest/contents.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### pytest example with setuptools\n",
    "$\\to$ See the directory `examples/setuptools/py-test` for an example on how to integrate tests into a package"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Version Control Systems, GIT\n",
    "\n",
    "* use version control for _any_ project\n",
    "    * Distributed development of large projects\n",
    "    * Small local single-person projects\n",
    "    * Useful during performance optimization\n",
    "* `git` is the tool of choice today  \n",
    "* good tutorial: https://git-scm.com/docs/gittutorial\n",
    "* [MPCDF GitLab](https://gitlab.mpcdf.mpg.de/) provides web-based repository services for collaboration\n",
    "    * git repository\n",
    "    * issue tracker\n",
    "    * wiki\n",
    "    * CI option for automated tests"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Continuous Integration\n",
    "\n",
    "* Tests can be executed when pushing commits\n",
    "* Tests should be small and fast\n",
    "* [Usage with gitlab](https://docs.gitlab.com/ce/ci/yaml/README.html): create `.gitlab-ci.yml`\n",
    "```yaml\n",
    "test-python:\n",
    "    script:\n",
    "      - pytest -v\n",
    "```\n",
    "* This will run the tests on every push event\n",
    "* Uses shared runners to execute the tests $\\to$ for special needs, separate runners can be used\n",
    "* Moreover, documentation can be generated automatically"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## PyPI, the Python Package Index\n",
    "* PyPI is the online software repository for Python\n",
    "* https://pypi.python.org/pypi\n",
    "* Contains nearly 590.000 packages (Nov 2024)\n",
    "* Zip files, tar balls, wheel archives are accepted  \n",
    "  (typically generated locally by front-end, e.g., `python -m build`)\n",
    "* Anybody may upload packages after registration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### PyPI (cont'd), pip\n",
    "\n",
    "* `pip` is used to download and install packages from PyPI  \n",
    "  `pip install --user PACKAGE`\n",
    "* `pip uninstall PACKAGE`\n",
    "* Software dependencies are resolved automatically"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Summary\n",
    "* Packaging, documentation, version control & testing facilitate development\n",
    "* Seems often like unnecessary overhead at first\n",
    "* *But:* pays off in the long run when projects become complex"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
