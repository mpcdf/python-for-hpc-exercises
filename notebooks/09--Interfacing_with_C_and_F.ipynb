{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Interfacing Python/NumPy with C/C++ and Fortran Code\n",
    "\n",
    "**Python for HPC course**\n",
    "\n",
    "Max Planck Computing and Data Facility, Garching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Motivation\n",
    "\n",
    "* Call compiled C/C++ or Fortran code from the Python layer\n",
    "    * Existing (legacy) code\n",
    "    * Newly developed and optimized code (numerical kernels)\n",
    "\n",
    "$\\rightarrow$ combine the convenience of using high-level Python with high-performance compiled code\n",
    "\n",
    "## Steps\n",
    "\n",
    "* create software interface between Python/NumPy and C/C++/Fortran\n",
    "* compile and link to become a Python-importable module $\\leftrightarrow$ usually integrated with packaging, \\\n",
    "  see `/notebooks/09--Interfacing_with_C_and_F.ipynb`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Interfacing Options\n",
    "\n",
    "We're looking into the following options:\n",
    "\n",
    "* Fortran $\\longrightarrow$ **f2py**\n",
    "* C/C++/CUDA $\\longrightarrow$ **Cython**\n",
    "* C++ $\\longrightarrow$ **pybind11**, **nanobind**\n",
    "\n",
    "Some other options (not covered here):\n",
    "\n",
    "* Swig, an interface generator\n",
    "* Boost.Python for C++ codes\n",
    "* Python's low-level C API"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## `f2py`: Interfacing with Fortran code\n",
    "\n",
    "* `f2py` \n",
    "    * is part of NumPy\n",
    "    * scans the Fortran code and generates signature files (.pyf)\n",
    "    * automatically takes care of type casting and non-contiguous arrays\n",
    "* two ways of usage\n",
    "    * direct calling of the `f2py` executable\n",
    "    * (define an extension in `setup.py` using `numpy.distutils`; **deprecated with python 3.12**)\n",
    "    * via an external build-system: `meson`, `cmake`, `scikit-build`, see (cf. the [f2py user guide](https://numpy.org/doc/stable/f2py/index.html))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Compile and link Fortran code to a Python module using `f2py`\n",
    "\n",
    "* Given the Fortran file `fib.f90`\n",
    "\n",
    "```Fortran\n",
    "subroutine fib(a,n)\n",
    "  integer, intent(in) :: n\n",
    "  integer(kind=8), intent(out) :: a(n)\n",
    "  do i=1,n\n",
    "     if (i.eq.1) then\n",
    "        a(i) = 0\n",
    "     elseif (i.eq.2) then\n",
    "        a(i) = 1\n",
    "     else \n",
    "        a(i) = a(i-1) + a(i-2)\n",
    "     endif\n",
    "  enddo\n",
    "end\n",
    "```\n",
    "\n",
    "* Generate 'fib' Python module file by calling `f2py -c fib.f90 -m fib` (second parameter is module name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Compiling and packaging with fortran sources using meson"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "```pyproject.toml\n",
    "# pyproject.toml\n",
    "[build-system]\n",
    "requires = ['meson-python', 'numpy']\n",
    "build-backend = 'mesonpy'\n",
    "\n",
    "[project]\n",
    "name = 'f2py_example'\n",
    "version = '1.0.0'\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* meson reads the build-config from `meson.build`, see `examples/f2py`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "```bash\n",
    "$ pip install .\n",
    "(...)\n",
    "$ python -c \"from f2p_example import fib; a=fib.fib(16); print(a[-1])\"\n",
    "610\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Cython: Interfacing with C/C++ code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Strategy\n",
    "* write a Cython extension that defines Python functions which call the external C code\n",
    "* see the example in the directory `cython/c_interface`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```C\n",
    "/* foobar/c_hello.h */\n",
    "void hello(void);\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "```C\n",
    "/* foobar/c_hello.c */\n",
    "#include <stdio.h>\n",
    "#include \"c_hello.h\"\n",
    "\n",
    "void hello(void) {\n",
    "    printf(\"Hello World!\\n\");\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "```cython\n",
    "# foobar/hello.pyx\n",
    "cdef extern from \"c_hello.h\":\n",
    "    void hello()\n",
    "\n",
    "def say_hello():\n",
    "    hello()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```pyproject.toml\n",
    "# pyproject.toml\n",
    "[build-system]\n",
    "requires = [\"setuptools\", \"cython\"]\n",
    "build-backend = \"setuptools.build_meta\"\n",
    "\n",
    "[project]\n",
    "name = \"foobar\"\n",
    "version = \"0.0.1\"\n",
    "\n",
    "[tool.setuptools]\n",
    "ext-modules = [\n",
    "  {name = \"foobar.hello\", sources = [\"src/foobar/hello.pyx\", \"src/foobar/c_hello.c\"]}\n",
    "]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Interfacing with C code from NumPy code using Cython\n",
    "\n",
    "* Goals\n",
    "  * Pass NumPy arrays down to C code to perform computation,  \n",
    "    and get the result back as a NumPy array\n",
    "  * Leave the memory management to the Python/NumPy layer\n",
    "* See the example in the directory `cython/c_numpy`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Tipps and tricks for interfacing NumPy with C\n",
    "\n",
    "* Cython layer\n",
    "    * check the datatypes to be passed carefully\n",
    "    * pass the pointer to the NumPy array (`a`) via `a.data` down to the C function\n",
    "    * handle (temporary) memory allocation conveniently via NumPy arrays here, not in the C layer\n",
    "    * make sure to only pass contiguous NumPy arrays to C, in case of non-contiguous views create a copy first\n",
    "\n",
    "* C layer\n",
    "    * write the extension stateless, beware of memory leaks\n",
    "    * you may use all kinds of code optimization techniques\n",
    "        * OpenMP threading (not affected by the Python GIL)\n",
    "        * vectorization\n",
    "        * cache optimization, etc.\n",
    "    * specify optimizing compiler flags according to the build-backend in use; e.g., with `setuptools` this is `pyroject.toml/setup.py`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Interfacing with a C library ('.so', shared object) using Cython\n",
    "* Goal: Use a C library from Python\n",
    "    * third-party library for which no Python bindings exist\n",
    "    * legacy code you want to wrap into Python\n",
    "    * CUDA code (compile into `.so` file independently using `nvcc`, first)\n",
    "* see the comprehensive example in `cython/c_interface_shared_object`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Tipps and tricks for interfacing shared objects\n",
    "* `setup.py`\n",
    "    * usability: locate the library and its headers, options\n",
    "        * installation location passed via environment variable,  \n",
    "          often the case on HPC systems via environment modules (`MKL_ROOT`, `GSL_HOME`, `FFTW_HOME`)\n",
    "        * installation location passed as arguments to `setup.py`\n",
    "        * installed at sytem location\n",
    "    * deployment: add the RPATH to the link line pointing to library location, to avoid `LD_LIBRARY_PATH`\n",
    "* hint: for CUDA code, it is often easier to go via a library than injecting `nvcc` into `setup.py`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Cython and C++\n",
    "\n",
    "* Cython supports many object-oriented/advanced C++ features: classes, templates, overloading\n",
    "* typical use case: write a Python wrapper class for a C++ class\n",
    "* not covered here, please see for further details  \n",
    "  http://cython.readthedocs.io/en/latest/src/userguide/wrapping_CPlusPlus.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Cython provides interfaces to libc and C++ STL (!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "%load_ext Cython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.8414709848078965\n"
     ]
    }
   ],
   "source": [
    "%%cython\n",
    "from libc.math cimport sin\n",
    "\n",
    "print(sin(1.0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]\n"
     ]
    }
   ],
   "source": [
    "%%cython\n",
    "# distutils: language = c++\n",
    "from libcpp.vector cimport vector\n",
    "\n",
    "cdef vector[int] v = range(10)\n",
    "print(v)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## pybind11\n",
    "\n",
    "* lightweight header-only library to create interfaces to modern C++ code\n",
    "* highlights: STL, iterators, classes and inheritance, smart pointers, move semantics, NumPy $\\leftrightarrow$ C++, Eigen $\\leftrightarrow$ Python, etc.\n",
    "* support for various build-systems/build-backends: **`cmake`**, `meson`, `cppimport`, `setuptools`\n",
    "* documentation: https://pybind11.readthedocs.io/en/latest/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Example integration with setuptools\n",
    "\n",
    "```toml\n",
    "[build-system]\n",
    "requires = [\"setuptools\", \"pybind11\"]\n",
    "build-backend = \"setuptools.build_meta\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Build with setuptools\n",
    "\n",
    "```python\n",
    "from setuptools import setup\n",
    "from pybind11.setup_helpers import Pybind11Extension, build_ext\n",
    "\n",
    "ext_modules = [\n",
    "    Pybind11Extension(\n",
    "        \"pybex\",\n",
    "        [\"src/pybex.cpp\",],\n",
    "    ),\n",
    "]\n",
    "\n",
    "setup(\n",
    "    cmdclass={\"build_ext\": build_ext},\n",
    "    ext_modules=ext_modules\n",
    ")\n",
    "```\n",
    "\n",
    "* cf. the simple NumPy example in `pybind11` for more details\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## nanobind\n",
    "\n",
    "* stripped down version of pybind11 but with signifact performance improvements both at run- and compile-time\n",
    "* can be built and packaged conveniently with `cmake` and `scikit-build-core`, see `examples/nanobind`\n",
    "* documentation: https://nanobind.readthedocs.io/en/latest/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
