{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Basics of High Performance Computing\n",
    "\n",
    "**Python for HPC course**\n",
    "\n",
    "Max Planck Computing and Data Facility, Garching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Outline\n",
    "\n",
    "* What is HPC?\n",
    "* HPC clusters\n",
    "* CPU\n",
    "* Memory access\n",
    "* Input/Output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## What is HPC?\n",
    "\n",
    "* High performance computing: \"computing at bottlenecks of the hardware\" (G. Hager, RRZE)\n",
    "* Write *efficient software* to use *maximum hardware potential*\n",
    "* Needed:\n",
    "    * Understand hardware\n",
    "    * Understand how software is executed on hardware\n",
    "* Common bottlenecks:\n",
    "    * Floating point operations\n",
    "    * Memory access\n",
    "    * Input/Output\n",
    "    * Communication\n",
    "* Here: only very basics, for more in-depth information, see courses from HLRS, LRZ and RRZE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Hardware\n",
    "\n",
    "Components of a typical HPC compute cluster:\n",
    "\n",
    "![cluster](fig/cluster_components.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Parallelism in a HPC cluster\n",
    "\n",
    "<img src=\"fig/cluster_parallelism.svg\" style=\"float:right; width:35%\" />\n",
    "\n",
    "Parallelism on multiple levels:\n",
    "* Cluster → nodes\n",
    "* Node → sockets (CPUs), GPUs\n",
    "* CPU → cores\n",
    "* Core → single instruction, multiple data (SIMD)\n",
    "\n",
    "More on exploiting parallelism later in the course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## CPU: central processing unit\n",
    "\n",
    "<img src=\"fig/cpu.svg\" style=\"float:right; width:35%\" />\n",
    "\n",
    "Simplified picture:\n",
    "* Front end:\n",
    "  * Fetch and decode instructions\n",
    "* Back end:\n",
    "  * Execute instructions on data\n",
    "  * Transfer data between registers and memory\n",
    "* Operates at a certain frequency\n",
    "* Processes a certain number\n",
    "  of operations per second\n",
    "\n",
    "Possible bottleneck: number of floating point operations per second (FLOPS)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Memory access\n",
    "\n",
    "<img src=\"fig/memory_hierarchy.svg\" style=\"float:right; width:35%\" />\n",
    "\n",
    "* HPC codes need data to work on\n",
    "* Data needs to be brought to the CPU\n",
    "* Memory much slower than CPU $\\to$ often a bottleneck\n",
    "* Mitigation: **memory hierarchy**\n",
    "* Cache is filled from memory in small chunks (cache lines)\n",
    "* Rules of thumb:\n",
    "    * Linear memory access\n",
    "    * Reuse memory as much as possible"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Usage of multi-D arrays\n",
    "\n",
    "<img src=\"fig/row-column-major.svg\" style=\"float:right; width: 25%\" />\n",
    "\n",
    "* Memory is 1D $\\to$ multi-D arrays need to be mapped\n",
    "* 2 possibilities:\n",
    "    * column-major (Fortran)\n",
    "    * row-major (C, Python)\n",
    "* Multiple loops:\n",
    "    * linear access wanted\n",
    "    * innermost loop: fastest changing index\n",
    "        * first index for column-major\n",
    "        * last index for row-major\n",
    "        \n",
    "<span style=\"font-size:small\">Image source: Wikipedia, Author: Cmglee</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Column vs. row major order\n",
    "\n",
    "* **Fortran example:** (column major)\n",
    "    ```Fortran\n",
    "    double precision :: a(N, M)\n",
    "    integer :: i, j\n",
    "    do j = 1, M\n",
    "      do i = 1, N\n",
    "        a(i, j) = i*j\n",
    "      end do\n",
    "    end do\n",
    "    ```\n",
    "\n",
    "* **C example:** (row major)\n",
    "    ```C\n",
    "    double a[N, M];\n",
    "    int i, j;\n",
    "    for (i = 0; i < N; i++)\n",
    "      for (j = 0; j < M; j++)\n",
    "        a[i, j] = i*j;\n",
    "\n",
    "    ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Python example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "152 ms ± 2.57 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)\n",
      "147 ms ± 5.62 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "N = 1000\n",
    "array = np.zeros((N, N))\n",
    "\n",
    "def set_array_row_major():\n",
    "    for i in range(N):\n",
    "        for j in range(N):\n",
    "            array[i, j] = i*j\n",
    "            \n",
    "def set_array_column_major():\n",
    "    for j in range(N):\n",
    "        for i in range(N):\n",
    "            array[i, j] = i*j\n",
    "\n",
    "%timeit set_array_row_major()\n",
    "%timeit set_array_column_major()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Difference not that big because of overhead in python\n",
    "\n",
    "$\\to$ try Cython!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "%load_ext Cython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%%cython -c=-O3 -c=-march=native\n",
    "import numpy as np\n",
    "cimport numpy as np\n",
    "cimport cython\n",
    "\n",
    "N = 10000\n",
    "\n",
    "@cython.boundscheck(False) # turn off bounds-checking for entire function\n",
    "@cython.wraparound(False)  # turn off negative index wrapping for entire function\n",
    "def set_array_row_major_cython():\n",
    "    cdef int i, j\n",
    "    cdef np.ndarray[np.float64_t, ndim=2] array = np.zeros((N, N), dtype=np.float64)\n",
    "    for i in range(N):\n",
    "        for j in range(N):\n",
    "            array[i, j] = i*j\n",
    "\n",
    "@cython.boundscheck(False) # turn off bounds-checking for entire function\n",
    "@cython.wraparound(False)  # turn off negative index wrapping for entire function\n",
    "def set_array_column_major_cython():\n",
    "    cdef int i, j\n",
    "    cdef np.ndarray[np.float64_t, ndim=2] array = np.zeros((N, N), dtype=np.float64)\n",
    "    for j in range(N):\n",
    "        for i in range(N):\n",
    "            array[i, j] = i*j"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "312 ms ± 3.06 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)\n",
      "1.5 s ± 112 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)\n"
     ]
    }
   ],
   "source": [
    "%timeit set_array_row_major_cython()\n",
    "%timeit set_array_column_major_cython()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Input and output\n",
    "HPC systems usually employ *parallel file systems*.\n",
    "* Goal: efficient I/O for large amounts of data written by many cores\n",
    "* Distribute files over many disks\n",
    "* Components:\n",
    "    * Storage servers\n",
    "    * Metadata servers\n",
    "* File access:\n",
    "    1. Get metadata from metadata server (**where** is it stored?)\n",
    "    2. Get data from storage servers\n",
    "* Done for every process for every file\n",
    "* Examples: GPFS, Lustre, BeeGFS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Best practice\n",
    "* Avoid I/O if possible\n",
    "* Often, metadata can be a bottleneck\n",
    "* Write **few big files**, do **not** write many small files\n",
    "* If accessing small files (e.g. configuration), do not read on every process\n",
    "  <br /> $\\to$ read on one process and distribute"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "## Containers in HPC\n",
    "* Promise of containers: portability\n",
    "* Possible performance problems:\n",
    "    * Hardware features (e.g. vector instructions)\n",
    "    * High-speed interconnect and MPI (access to low-level drivers)\n",
    "* Best performance requires compilation on each system (native compiler and MPI)\n",
    "* docker not supported on MPCDF systems\n",
    "* Solutions for HPC: singularity/apptainer, charliecloud"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## What is HPC about?\n",
    "\n",
    "* Write programs that use the hardware to the limits\n",
    "* Be aware of the bottlenecks of the hardware\n",
    "    * FLOPS\n",
    "    * Memory access\n",
    "    * Input/output\n",
    "    * Communication\n",
    "* Some basic rules:\n",
    "    * **Access memory linearly!**\n",
    "    * Write few, big files"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
