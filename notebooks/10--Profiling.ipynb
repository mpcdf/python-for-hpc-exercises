{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Profiling\n",
    "**Python for HPC course**\n",
    "\n",
    "Max Planck Computing and Data Facility, Garching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Argh! My code is very slow. I need to optimize it.\n",
    "\n",
    "\n",
    "But where do I even start? And what does \"slow\" mean? How fast could the code be?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "I need to analyze my code before I start optimizing. Let's call it \"profiling\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## What is profiling?\n",
    "* Profiling: *measure specified performance metrics for different parts of a code*\n",
    "* Performance metrics can be time spent, memory footprint, GFlops, ...\n",
    "* Code parts: functions, loops, source lines, ...\n",
    "\n",
    "\n",
    "* Critical step: understand code behavior before deciding where to focus optimization efforts\n",
    "* “Premature optimization is the root of all evil” (Donald Knuth)\n",
    "* Pareto rule: “80% of the gains generally come from focusing on 20% of the code”"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Simple optimization methodology\n",
    "* Profile timing: get list of code parts sorted by the time needed\n",
    "* Start optimization\n",
    "    * Focus on code part taking most time $\\to$ largest benefit\n",
    "* Profile again and tackle next item on the list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Profiling python code\n",
    "* Methodology depends on your code\n",
    "    * Python code (native, NumPy, SciPy)\n",
    "    * Python code with compiled extensions\n",
    "* Different approaches and tools\n",
    "    * Different metrics (time, number of calls, GFlops, memory bandwidth, ...)\n",
    "    * Different code parts (self-defined, functions, lines, ...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Timing\n",
    "### A simple timing approach"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.019829 s\n"
     ]
    }
   ],
   "source": [
    "import time\n",
    "import numpy as np\n",
    "\n",
    "t0 = time.time()\n",
    "M = np.random.rand(1024,1024)\n",
    "t1 = time.time()\n",
    "\n",
    "dt = t1 - t0\n",
    "print(\"%f s\" % dt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "$\\to$ get time for specific parts of the code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Jupyter's per-cell timing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 17.4 ms, sys: 3.5 ms, total: 20.9 ms\n",
      "Wall time: 19.8 ms\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "import numpy as np\n",
    "M = np.random.rand(1024,1024)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "$\\to$ compare different parts of the algorithm/pipeline easily"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### timeit module\n",
    "* timeit runs a function several times to get statistics about its runtime\n",
    "* Part of the standard library, usable from the command line  \n",
    "  https://docs.python.org/3/library/timeit.html\n",
    "* Convenient usage from Jupyter notebook or ipython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "8.97 ms ± 250 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "def my_function():\n",
    "    return np.random.rand(1024,1024)\n",
    "\n",
    "%timeit my_function()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "$\\to$ compare timings of different functions easily with good statistics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## cProfile\n",
    "* cProfile is part of the standard library  \n",
    "  https://docs.python.org/3/library/profile.html\n",
    "* Invocation via the command line, e.g.  \n",
    "  `python -m cProfile -s time program.py`\n",
    "* Measures time per function (self time and cumulative time) and number of calls\n",
    "* See the example in the folder `profiling`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### cProfile output sort order\n",
    "* Useful options\n",
    "    * calls\n",
    "    * time\n",
    "    * cumtime\n",
    "* https://docs.python.org/3/library/profile.html#pstats.Stats.sort_stats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Using cProfile\n",
    "* invocation within Python code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import cProfile, pstats\n",
    "profile = cProfile.Profile()\n",
    "profile.enable()   # --- start profiling\n",
    "M = np.random.rand(1024,1024)\n",
    "S = np.sin(M)\n",
    "profile.disable()  # --- stop profiling\n",
    "profile.create_stats()\n",
    "with open(\"profile.txt\", 'w') as fp:\n",
    "    stats = pstats.Stats(profile, stream=fp)\n",
    "    stats.sort_stats('time')\n",
    "    stats.print_stats()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "         29 function calls in 0.125 seconds\n",
      "\n",
      "   Ordered by: internal time\n",
      "\n",
      "   ncalls  tottime  percall  cumtime  percall filename:lineno(function)\n",
      "        1    0.075    0.075    0.075    0.075 <ipython-input-1-06edbb1bb81e>:6(<module>)\n",
      "        1    0.049    0.049    0.049    0.049 {method 'rand' of 'mtrand.RandomState' objects}\n",
      "        3    0.000    0.000    0.125    0.042 /home/sohlmann/.local/lib/python3.6/site-packages/IPython/core/interactiveshell.py:2933(run_code)\n",
      "        3    0.000    0.000    0.000    0.000 {built-in method builtins.compile}\n",
      "        3    0.000    0.000    0.000    0.000 /usr/lib/python3.6/codeop.py:132(__call__)\n",
      "        3    0.000    0.000    0.000    0.000 /home/sohlmann/.local/lib/python3.6/site-packages/IPython/core/hooks.py:142(__call__)\n",
      "        3    0.000    0.000    0.125    0.042 {built-in method builtins.exec}\n",
      "        3    0.000    0.000    0.000    0.000 /home/sohlmann/.local/lib/python3.6/site-packages/IPython/utils/ipstruct.py:125(__getattr__)\n",
      "        1    0.000    0.000    0.049    0.049 <ipython-input-1-06edbb1bb81e>:5(<module>)\n",
      "        1    0.000    0.000    0.000    0.000 <ipython-input-1-06edbb1bb81e>:7(<module>)\n",
      "        3    0.000    0.000    0.000    0.000 /home/sohlmann/.local/lib/python3.6/site-packages/IPython/core/interactiveshell.py:1104(user_global_ns)\n",
      "        3    0.000    0.000    0.000    0.000 /home/sohlmann/.local/lib/python3.6/site-packages/IPython/core/hooks.py:207(pre_run_code_hook)\n",
      "        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}\n",
      "\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "<pstats.Stats at 0x7f9d844264e0>"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "stats = pstats.Stats(profile)\n",
    "stats.sort_stats('time')\n",
    "stats.print_stats()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## line_profiler\n",
    "* `line_profiler` allows to measure the cost of each single line of code\n",
    "* Obtain `line_profiler` first via  \n",
    "  `pip install --user line_profiler`\n",
    "* See the example in the folder `profiling`\n",
    "* https://github.com/rkern/line_profiler"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Using line_profiler\n",
    "* Functions to be profiled need to be decorated with `@profile`\n",
    "* To run a decorated code independent of line_profiler, you may define a stub decorator, e.g."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# provide a stub for '@profile' in case line_profiler is not used\n",
    "import builtins\n",
    "try:\n",
    "    builtins.profile\n",
    "except AttributeError:\n",
    "    def profile(func):\n",
    "        return func\n",
    "    builtins.profile = profile"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Profiling of compiled extensions\n",
    "* Problem: previously discussed Python tools cannot look into the compiled layer\n",
    "* Options\n",
    "    * implement timing output in the compiled code yourself (`printf()`)\n",
    "    * ltrace\n",
    "    * perf\n",
    "    * Intel Amplifier XE (VTUNE)\n",
    "    * gprof (not shown here)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## ltrace\n",
    "* ltrace is a library call tracer for Linux\n",
    "* example usage, assuming you want to trace the extension 'c_bioen.so'  \n",
    "  `ltrace -x @c_bioen.so -c python script.py`\n",
    "* Output:  \n",
    "\n",
    "```text\n",
    "% time     seconds  usecs/call     calls      function\n",
    "------ ----------- ----------- --------- --------------------\n",
    " 30.20    0.303325      303325         1 __pyx_pw_5bioen_3ext_7c_bioen_59bioen_opt_bfgs_forces\n",
    " 30.10    0.302320      302320         1 _opt_bfgs_forces\n",
    "  5.87    0.058993        6554         9 _bioen_log_posterior_forces\n",
    "  5.14    0.051609        7372         7 _grad_bioen_log_posterior_forces_interface\n",
    "  4.75    0.047726        2982        16 _get_weights_from_forces\n",
    "  4.66    0.046842        6691         7 _grad_bioen_log_posterior_forces\n",
    "  4.25    0.042673        5334         8 _bioen_log_posterior_forces_interface\n",
    "  3.68    0.036947        2309        16 _get_weights_from_forces._omp_fn.1\n",
    "  3.58    0.035986       35986         1 fdf_forces\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## perf - a performance profiler for Linux systems\n",
    "* Allows access to e.g. programmable hardware counters (e.g. for FLOPs)\n",
    "* Useful commands\n",
    "    * `perf help`, `perf list`\n",
    "    * `perf stat`: get performance counter statistics\n",
    "    * `perf record`: run application and collect profile\n",
    "    * `perf report -g`: explore recorded profile, get callgraph, identify hotspots, see assembly\n",
    "    * use Brendan Gregg's flamegraph visualization    \n",
    "* Python >= 3.12: [native perf support](https://docs.python.org/3/howto/perf_profiling.html)\n",
    "* References, tutorials\n",
    "    * See the example in the folder `profiling`\n",
    "    * http://www.brendangregg.com/perf.html\n",
    "    * https://github.com/brendangregg/FlameGraph\n",
    "    * https://perf.wiki.kernel.org/index.php/Main_Page"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### flamegraph visualization of perf data\n",
    "![flame graph](fig/perf_flame_graph.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Intel VTUNE (formerly Amplifier XE)\n",
    "* Powerful profiler to obtain information on, e.g.\n",
    "    * Hotspots\n",
    "    * Memory bandwidth\n",
    "    * Vectorization, threading\n",
    "* Initially developed to profile compiled code (C/C++, Fortran)\n",
    "* Recent versions seamlessly support Python and compiled extensions\n",
    "* Available on MPCDF systems via the environment module `vtune`\n",
    "* Invocation directly within the GUI, `vtune-gui` (previously: `amplxe-gui`)\n",
    "* Or on the command line, `vtune -collect hotspots ./diff_3_numpy.py` (previously: `amplxe-cl`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### VTUNE options\n",
    "Depending on the analysis type (e.g. \"hotspots\"), a plethora of information is available, the most important being\n",
    "* Call tree including the cost, covering Python and compiled code\n",
    "* Summary on threading and hotspots\n",
    "* View assembly of compiled code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![vtune tree](fig/VTUNE_Python_Tree.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![vtune tree](fig/VTUNE_Python_Summary.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![vtune tree](fig/VTUNE_Python_Assembly.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Profiling memory usage\n",
    "\n",
    "* Sometimes, memory usage can be an issue (out-of memory)\n",
    "* Good tool: [memray](https://github.com/bloomberg/memray)\n",
    "* Usage:\n",
    "    1. Create a profile with `memray -o profile.out yourscript.py`\n",
    "    2. Summary with `memray summary profile.out`\n",
    "    2. Flame graph with `memray flamegraph profile.out`; this creates an html file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "![memray](fig/memray_flamegraph.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Profiling: Summary\n",
    "* Critical to understand code behavior before deciding where to focus optimization efforts\n",
    "* Always profile before and during optimization\n",
    "* Different approaches\n",
    "    * Pure python code & extensions\n",
    "    * Different metrics\n",
    "    * Different code parts"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
