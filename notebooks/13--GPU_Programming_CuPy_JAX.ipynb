{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# GPU programming with Python\n",
    "**Python for HPC course**\n",
    "\n",
    "Max Planck Computing and Data Facility, Garching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## GPUs\n",
    "* [GPU architecture in a nutshell](https://juser.fz-juelich.de/record/943414/files/aherten-ubonn-gpus.pdf)\n",
    "    * massively parallel processors (streaming multiprocessors) with many lightweight cores each  \n",
    "      $\\rightarrow$ capable of running thousands of threads in parallel (SIMT), optimized to hide memory latency\n",
    "    * very high memory bandwidth, in particular with on-chip high-bandwidth-memory (HBM2)\n",
    "    * specification example: [NVIDIA A100 GPU](https://www.nvidia.com/content/dam/en-zz/Solutions/Data-Center/a100/pdf/nvidia-a100-datasheet-us-nvidia-1758950-r4-web.pdf) on [MPCDF Raven](https://docs.mpcdf.mpg.de/doc/computing/raven-user-guide.html)\n",
    "        * double-precision performance approximately 10 TFLOP/s, much higher for lower precision\n",
    "        * memory bandwidth nearly 1.6 TB/s\n",
    "    * [GPUs are accelerator devices attached to a host computer](https://docs.mpcdf.mpg.de/doc/computing/raven-details.html), data needs to go through a bus system (but this is changing with recent architectures)\n",
    "* well suited for parallel numerical computations on large arrays, provided that transfer overhead is small compared to the cost of the computations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## GPU options for Python programmers (overview, selection)\n",
    "\n",
    "* use plain CUDA and implement a Python wrapper in C/C++ using Cython or pybind11\n",
    "* [pycuda](https://github.com/inducer/pycuda) (and similarly [pyopencl](https://pypi.org/project/pyopencl/))\n",
    "    * wraps native CUDA kernels into Python\n",
    "    * you need to know CUDA programming ($\\rightarrow$ `examples/gpu/pycuda`)\n",
    "* [Numba](https://numba.readthedocs.io/en/stable/)\n",
    "    * just-in-time compilation for GPUs\n",
    "    * you need to know at least the basic ideas of CUDA to write GPU code\n",
    "* [CuPy](https://cupy.dev)\n",
    "    * drop-in replacement for NumPy on GPUs\n",
    "    * no GPU programming knowledge required\n",
    "* [Jax](https://github.com/google/jax)\n",
    "    * just-in-time compilation of NumPy expressions on GPUs\n",
    "    * no GPU programming knowledge required, some Jax knowledge required"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Numba for GPUs\n",
    "\n",
    "* Numba wraps most of CUDA's functionality into Python\n",
    "* jit-compile GPU kernel functions using decorators\n",
    "    * `@cuda.jit`, target NVIDIA CUDA GPUs\n",
    "    * `@roc.jit`, target AMD GPUs\n",
    "* express loops in terms of a grid of thread blocks (like in plain CUDA)\n",
    "* by default, data is copied back to host automatically after a kernel finishes,  \n",
    "  but explicit memory management is possible as well (host arrays vs. device arrays)\n",
    "* see `examples/gpu/numba_gpu`\n",
    "* https://numba.readthedocs.io/en/stable/cuda/index.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```python\n",
    "# Numba CUDA example\n",
    "import numpy as np\n",
    "from numba import cuda\n",
    "\n",
    "@cuda.jit\n",
    "def increment_by_one(a):\n",
    "    # thread id, blocks, etc. same as in native CUDA\n",
    "    tx, ty, bw = cuda.threadIdx.x, cuda.blockIdx.x, cuda.blockDim.x\n",
    "    pos = tx + ty * bw\n",
    "    if pos < a.size:  # check array boundaries\n",
    "        a[pos] += 1\n",
    "\n",
    "x = np.arange(2**20, dtype=np.float32)\n",
    "threadsperblock = 32\n",
    "blockspergrid = (x.size + (threadsperblock - 1)) // threadsperblock\n",
    "increment_by_one[blockspergrid, threadsperblock](x)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## CuPy\n",
    "* drop-in replacement for NumPy and SciPy on AMD and NVIDIA GPUs\n",
    "    * `cupy.ndarray` for various datatypes including advanced indexing and broadcasting\n",
    "    * sparse matrices\n",
    "    * NumPy routines, linear algebra, FFT, RNG\n",
    "    * SciPy routines\n",
    "* implementation uses high-performance backend libraries (e.g. cuBLAS, cuFFT, cuSPARSE, ...)\n",
    "* calls to CuPy's NumPy and SciPy functions are asynchronous, some operations (memcopies) are synchronous\n",
    "* https://docs.cupy.dev/en/stable/index.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### CuPy basics\n",
    "* L2 norm calculation on the CPU using NumPy\n",
    "```python\n",
    "import numpy as np\n",
    "x_cpu = np.array([1, 2, 3])\n",
    "l2_cpu = np.linalg.norm(x_cpu)\n",
    "```\n",
    "* L2 norm calculation on the GPU using CuPy\n",
    "```python\n",
    "import cupy as cp\n",
    "x_gpu = cp.array([1, 2, 3])\n",
    "l2_gpu = cp.linalg.norm(x_gpu)\n",
    "```\n",
    "* CuPy implements NumPy functionality on `cupy.ndarray` objects.\n",
    "* The arrays `x_gpu` and `l2_gpu` are located in GPU memory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### CuPy memory management\n",
    "* `cupy.ndarray` is always located in GPU memory\n",
    "* copy from the host to the device\n",
    "```python\n",
    "x_cpu = np.array([1, 2, 3])  # create an array on the host\n",
    "x_gpu = cp.asarray(x_cpu)  # copy the data to the current device.\n",
    "```\n",
    "* copy from the device to the host\n",
    "```python\n",
    "x_gpu = cp.array([1, 2, 3])  # create an array on the current device\n",
    "x_cpu = cp.asnumpy(x_gpu)  # copy the array to the host\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### CuPy advanced features\n",
    "* user-defined kernels: element-wise, reduction, raw\n",
    "* multiple devices\n",
    "* streams\n",
    "* profiling\n",
    "* support for CUDA-aware MPI and NCCL\n",
    "* support for Numba CUDA kernels\n",
    "* import/export of raw memory pointers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### CuPy FFT example\n",
    "* full runnable example at `examples/gpu/fft_cupy_jax`\n",
    "```python\n",
    "# code snippet, a is a numpy array with complex numbers\n",
    "a_d = cp.asarray(a)  # transfer input to device\n",
    "with scipy.fft.set_backend(cufft):\n",
    "    b_d = scipy.fft.fft(a_d)  # run fft on device\n",
    "    b_h = cp.asnumpy(b_d)  # transfer result to host\n",
    "```\n",
    "* comparison of CuPy FFT to standard SciPy FFT (Anaconda Python)\n",
    "```text\n",
    "GPU 0: NVIDIA A100-SXM4-40GB\n",
    "FFT input array size: 4.0 GiB\n",
    "SciPy: 12.700828631718954 s\n",
    "CuPy:  0.4057832558949788 s\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Jax\n",
    "* jax is a library for numerical computing and machine learning, offers a NumPy interface (cf. `Numba_Jax_JIT.ipynb` for introductory details)\n",
    "* jax provides just-in-Time (JIT) compilation using XLA and automatic differentiation on GPUs, TPUs, and CPUs\n",
    "* calls to `jax.numpy` functions are non-blocking (\"asynchronous dispatch\")\n",
    "    * accessing the output from the host Python code blocks until the actual output is available\n",
    "    * call `.block_until_ready()` to explicitly force the completion of a function call\n",
    "    * use `.device.put()` and `.device.get()` to control memory transfers explicitly\n",
    "* https://jax.readthedocs.io/en/latest/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### JAX FFT example\n",
    "* full runnable example at `examples/gpu/fft_cupy_jax`\n",
    "```python\n",
    "import jax\n",
    "import jax.numpy as jnp\n",
    "# code snippet, a is a numpy array with complex numbers\n",
    "a_d = jax.device_put(a)\n",
    "b_d = jnp.fft.fft(a_d)\n",
    "b_h = jax.device_get(b_d)\n",
    "```\n",
    "* comparison of JAX FFT to standard SciPy FFT (Anaconda Python) and CuPy\n",
    "```text\n",
    "GPU 0: NVIDIA A100-SXM4-40GB\n",
    "FFT input array size: 4.0 GiB\n",
    "SciPy: 12.684897343317667 s\n",
    "CuPy:  0.40602771441141766 s\n",
    "JAX:   0.40358416239420575 s\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## GPU summary\n",
    "* GPUs are very powerful, but not all problems can be solved efficiently on GPUs\n",
    "    * understand your computational task well and profile your NumPy code carefully when considering to move to GPUs\n",
    "    * on the GPU, measure timings and put them into perspective by comparing to a good multithreaded CPU implementation\n",
    "* CuPy and Jax offer a very low entry-barrier to using GPUs, requiring virtually only NumPy knowledge and no low-level CUDA knowledge"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
