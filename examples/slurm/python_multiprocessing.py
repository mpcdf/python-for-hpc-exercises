#!/usr/bin/env python

import os, sys
import multiprocessing as mp

def my_f(x):
  """Function to be run in parallel.
  """
  print("{} : {}".format(x, os.getpid()))

# set the number of processes to be used
try:
    np = int(sys.argv[1]) # ... as passed in via the command line, if possible
except:
    np = mp.cpu_count()   # ... else as detected by multiprocessing

with mp.Pool(np) as p:
  p.map(my_f, range(np))
