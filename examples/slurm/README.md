# Examples for Python Slurm scripts for MPCDF HPC systems

## Small programs and problem sizes

* sequential program

## Full-node setups for larger problems

* multithreaded program using NumPy/MKL
* parallel program using Python's multiprocessing (restricted to a single node)
* parallel program using MPI for Python (mpi4py, multi-node)

Important: Before using full nodes, please check that your program actually
benefits from these powerful resources, otherwise use shared nodes.

Find up-to-date documentation on Slurm and related topics at:
https://docs.mpcdf.mpg.de/doc/computing/index.html

