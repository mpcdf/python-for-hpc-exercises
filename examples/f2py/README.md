# Minimal example on how-to interface with Fortran code using 'f2py'

* build python package using the meson build-system, `pip install .`
* test the module using 'test_fibonacci.py'.

* to get a standalone python module run  `compile_with_f2py`
