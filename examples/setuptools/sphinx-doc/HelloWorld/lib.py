# implementation file, part of the "HelloWorld" demo Python package

def say_hello():
    """Say friendly hello!

    This functions says hello to you.

    Parameters
    ----------
    None

    Returns
    -------
    Nothing
    """
    print("Hello World!")
