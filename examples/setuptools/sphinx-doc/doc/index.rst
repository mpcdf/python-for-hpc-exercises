.. HelloWorld documentation master file, created by
   sphinx-quickstart on Fri Nov 16 14:59:36 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HelloWorld's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Introduction
------------

HelloWorld is an amazing project!


Automatically generated documentation
-------------------------------------

.. automodule:: HelloWorld.lib
   :members:
   :undoc-members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
