{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Python for Scientific Computing\n",
    "## Max Planck Computing and Data Facility, Garching"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Example: Heat Equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "* toy problem: (constant) diffusion on a 2d periodic grid $u_t = \\alpha \\Delta u$\n",
    "* explicit time stepping\n",
    "* **lecture example**\n",
    "    * finite difference Laplacian operator, 5-point stencil\n",
    "    * formula for time update of $u$ at position $x_i,y_j$ from time $t_n$ to $t_{n+1}$: \n",
    "    $$ u_{i,j}^{(n+1)} = u_{i,j}^{(n)} + \\frac{\\alpha \\Delta t}{\\Delta x^2} \\left( \n",
    "    u_{i-1,j}^{(n)}\n",
    "    +u_{i+1,j}^{(n)}\n",
    "    +u_{i,j-1}^{(n)}\n",
    "    +u_{i,j+1}^{(n)}\n",
    "    -4u_{i,j}^{(n)}\n",
    "    \\right) $$\n",
    "* **exercise**\n",
    "    * use a 9-point stencil finite difference Laplacian operator\n",
    "    * formula for time update of $u$ at position $x_i,y_j$ from time $t_n$ to $t_{n+1}$: \n",
    "    $$ u_{i,j}^{(n+1)} = u_{i,j}^{(n)} + \\frac{\\alpha \\Delta t}{3 \\Delta x^2} \\left( \n",
    "      u_{i+1,j}^{(n)} + u_{i-1,j}^{(n)} + u_{i,j+1}^{(n)} + u_{i,j-1}^{(n)}\n",
    "    + u_{i+1,j+1}^{(n)} + u_{i-1,j-1}^{(n)} + u_{i+1,j-1}^{(n)} + u_{i-1,j+1}^{(n)} - 8u_{i,j}^{(n)}\n",
    "    \\right) $$\n",
    "* implemente various approaches\n",
    "    * naive Python implementation\n",
    "    * NumPy variants\n",
    "    * Cython\n",
    "    * Numba\n",
    "* measure, compare, improve"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Technical detail: limit the number of threads to allow for a fair comparison,\n",
    "#                   some NumPy expressions would try to parallelize automatically\n",
    "import os\n",
    "for VAR in [\"OMP_NUM_THREADS\", \"NUMBA_NUM_THREADS\"]:\n",
    "    if VAR not in os.environ:\n",
    "        os.environ[VAR]=\"2\"\n",
    "    print(\"{}={}\".format(VAR, os.environ[VAR]))\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Infrastructure code: parameters, init(), main_loop()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# parameters (global, for convenience)\n",
    "n_iterations = 50\n",
    "n_points = 254\n",
    "dt = 0.12\n",
    "D = 0.8"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def init(val=0.5):\n",
    "    \"\"\"Set up a 2d NumPy array with some initial value pattern.\"\"\"\n",
    "    x = np.linspace(0., 4.*np.pi, num=n_points+2)\n",
    "    y = np.linspace(0., 4.*np.pi, num=n_points+2)\n",
    "    grid = val * np.outer(np.sin(x)**4, np.sin(y)**4)\n",
    "    return grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def main_loop(evolve_func, grid):\n",
    "    \"\"\"Main loop function, calling evolve_func on grid.\"\"\"\n",
    "    grid_tmp = np.empty_like(grid)\n",
    "    for i in range(1, n_iterations+1):\n",
    "        evolve_func(grid, grid_tmp, n_points, dt, D)\n",
    "        # swap references, do not copy\n",
    "        grid_foo = grid\n",
    "        grid = grid_tmp\n",
    "        grid_tmp = grid_foo\n",
    "    return grid"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## 1. A naive pure Python implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def apply_periodic_bc_python(grid, n_points):\n",
    "    \"\"\"Explicitly apply periodic boundary conditions, via Python loops.\"\"\"\n",
    "    for j in range(n_points + 2):\n",
    "        grid[ 0, j] = grid[-2, j]\n",
    "        grid[-1, j] = grid[ 1, j]\n",
    "    for i in range(n_points + 2):\n",
    "        grid[ i,-1] = grid[ i, 1]\n",
    "        grid[ i, 0] = grid[ i,-2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def evolve_python(grid, grid_tmp, n_points, dt, D):\n",
    "    \"\"\"Time-step using 5-pt stencil, as presented in the lecture.\"\"\"\n",
    "    apply_periodic_bc_python(grid, n_points)\n",
    "    for i in range(1, n_points+1):\n",
    "        for j in range(1, n_points+1):\n",
    "            # stencil formula\n",
    "            grid_tmp[i, j] = grid[i, j] + dt * D * (\n",
    "                grid[i-1, j] + grid[i+1, j] + grid[i, j-1] + grid[i, j+1]\n",
    "                - 4.0 * grid[i, j])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 5.38 s, sys: 2.72 ms, total: 5.38 s\n",
      "Wall time: 19.1 s\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "grid = init()\n",
    "solution_python = main_loop(evolve_python, grid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def evolve_python_nine(grid, grid_tmp, n_points, dt, D):\n",
    "    \"\"\"Time-step using 9-pt stencil.\"\"\"\n",
    "    apply_periodic_bc_python(grid, n_points)\n",
    "    for i in range(1, n_points+1):\n",
    "        for j in range(1, n_points+1):\n",
    "            # stencil formula\n",
    "            # ---> add 9pt stencil here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 9.15 s, sys: 20.4 ms, total: 9.17 s\n",
      "Wall time: 28.8 s\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "grid = init()\n",
    "solution_python_nine = main_loop(evolve_python_nine, grid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "def plot_grids(grid_1, grid_2):\n",
    "    f = plt.figure(figsize=(10,5))\n",
    "    plt.subplot(121)\n",
    "    plt.pcolormesh(grid_1.T)\n",
    "    plt.axis('image')\n",
    "    plt.title('grid_1')\n",
    "    plt.subplot(122)\n",
    "    plt.pcolormesh(grid_2.T)\n",
    "    plt.axis('image')\n",
    "    l = plt.title('grid_2')\n",
    "\n",
    "plot_grids(solution_python, solution_python_nine)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.00102803880472\n"
     ]
    }
   ],
   "source": [
    "# look at the deviation of the solutions between the 5pt and the 9pt stencil\n",
    "rel = np.max(np.abs(solution_python - solution_python_nine))/np.max(np.abs(solution_python))\n",
    "print(rel)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## 2. NumPy implementations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "def evolve_numpy_slicing_nine(grid, grid_tmp, n_points, dt, D):\n",
    "    \"\"\"Time-step using 9-pt stencil.\"\"\"\n",
    "    apply_periodic_bc_python(grid, n_points)\n",
    "    # stencil formula\n",
    "    # ---> add 9pt stencil in NumPy notation here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 65.5 ms, sys: 0 ns, total: 65.5 ms\n",
      "Wall time: 191 ms\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "grid = init()\n",
    "solution_numpy_slicing_nine = main_loop(evolve_numpy_slicing_nine, grid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "plot_grids(solution_python, solution_numpy_slicing_nine)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## 3. Cython implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%load_ext Cython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%%cython -c=-O3 -c=-march=native -c=-ffast-math \n",
    "# --annotate\n",
    "\n",
    "import numpy as np\n",
    "cimport numpy as np\n",
    "cimport cython\n",
    "\n",
    "# --- add performance decorators\n",
    "cpdef apply_periodic_bc_cython(grid, n_points):\n",
    "    # --- start with the plain Python code and add type declarations, etc.\n",
    "    \n",
    "# --- add performance decorators\n",
    "def evolve_cython_nine(grid,\n",
    "                       grid_tmp,\n",
    "                       n_points, dt, D):\n",
    "    apply_periodic_bc_cython(grid, n_points)\n",
    "    # --- start with the plain Python code and add type declarations, etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 14.7 ms, sys: 138 µs, total: 14.9 ms\n",
      "Wall time: 64.8 ms\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "grid = init()\n",
    "solution_cython_nine = main_loop(evolve_cython_nine, grid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "plot_grids(solution_python, solution_cython_nine)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## 4. Numba implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from numba import jit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# add decorator here\n",
    "def apply_periodic_bc_numba(grid, n_points):\n",
    "    \"\"\"Explicitly apply periodic boundary conditions.\"\"\"\n",
    "    # copy Python implementation here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# add decorator here\n",
    "def evolve_numba_nine(grid, grid_tmp, n_points, dt, D):\n",
    "    \"\"\"Time-step using 9-pt stencil.\"\"\"\n",
    "    apply_periodic_bc_numba(grid, n_points)\n",
    "    # copy Python implementation here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# run once without timing in order to compile, do the timing below\n",
    "grid = init()\n",
    "solution_numba_nine = main_loop(evolve_numba_nine, grid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPU times: user 880 µs, sys: 3.81 ms, total: 4.69 ms\n",
      "Wall time: 14.3 ms\n"
     ]
    }
   ],
   "source": [
    "%%time\n",
    "grid = init()\n",
    "solution_numba_nine = main_loop(evolve_numba_nine, grid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "plot_grids(solution_python, solution_numba_nine)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Optional exercise parts\n",
    "* Further increase the order/stencil width of the Laplacian (requires to increase the width of the ghost cells for the periodic boundary conditions)\n",
    "* Implement functions to write the grid every few time steps to a HDF5 file (and to read it in again)\n",
    "* Implement a restart functionality: restart the simulation from a saved HDF5 file\n",
    "* Implement reading in a parameter file in YAML format for the simulation parameters (number of iterations, number of points, timestep...)\n",
    "* Complete the MPI implementation in `Diffusion_MPI.py` that uses a domain decomposition approach (complete communication of boundaries and communication of whole grid to root for plotting)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
