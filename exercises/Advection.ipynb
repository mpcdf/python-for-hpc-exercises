{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise: solving the advection equation\n",
    "\n",
    "## Problem setting\n",
    "* Our goal is to solve the 1D advection equation $$u_t + a u_x = 0$$ for \n",
    "$u(x,t):\\mathbb{R}\\times\\mathbb{R}^+\\rightarrow\\mathbb{R}$ and $a>0$\n",
    "* This is an hyperbolic PDE\n",
    "* We use the Lax-Friedrichs method defined by\n",
    "$$\n",
    "  U_j^{n+1} = \\frac{1}{2}\\left( U_{j-1}^n + U_{j+1}^n \\right) \n",
    "  - \\frac{\\Delta t}{2\\Delta x} a \\left(U_{j+1}^n - U_{j-1}^n \\right),\n",
    "$$\n",
    "where $U_j^{n} = u(x_j, t_n)$ and we assume an equidistant grid\n",
    "$x_j = j \\Delta x$, $t_n = n \\Delta t$.\n",
    "* This method can be derived either in a finite-volume or finite-difference framework.\n",
    "* This method is stable (i.e., errors do not grow) for\n",
    "$$\n",
    "\\left| \\frac{\\Delta t}{\\Delta x} a \\right| < 1\n",
    "$$\n",
    "or \n",
    "$$\n",
    "\\Delta t < \\left| \\frac{\\Delta x}{a} \\right|\n",
    "$$\n",
    "* Assume periodic boundary conditions.\n",
    "\n",
    "## Exercise\n",
    "1. Complete the python implementation.\n",
    "2. Speed the code up by using operations over numpy arrays.\n",
    "3. Reach a further speed-up by using Cython and Numba.\n",
    "4. Examine properties of the solver for different initial conditions (e.g. 0 for $x<0$ and 1 for $x>0$ and different CFL values (e.g., $cfl=0.2$; what happens for $cfl>1$?).\n",
    "5. Extend to 2D."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General code infrastructure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "class Mesh:\n",
    "    def __init__(self, Nx=1000, L=10, a=2, cfl=0.9, ic_type=1, do_plot=True):\n",
    "        self.Nx = Nx\n",
    "        self.Nx_total = Nx + 2\n",
    "        self.deltax = 2.*L/Nx\n",
    "        # include one boundary cell at each end\n",
    "        self.x = np.arange(-1, Nx+1) * self.deltax - L\n",
    "        # ensure stability criterion is fulfilled\n",
    "        self.deltat = np.abs(self.deltax / a) * cfl\n",
    "        # multiplication factor for time evolution\n",
    "        self.alpha = a * self.deltat / (2.*self.deltax)\n",
    "        self.set_initial_conditions(ic_type)\n",
    "        self.u_tmp = np.zeros_like(self.u)\n",
    "        if do_plot:\n",
    "            self.plotter = Plotter()\n",
    "        \n",
    "    def set_initial_conditions(self, type=1):\n",
    "        if type==1:\n",
    "            self.u = np.zeros_like(self.x)\n",
    "            self.u[self.x <= 0.0] = 1.0\n",
    "        else:\n",
    "            raise ValueError(\"Type {:d} not known.\".format(type))\n",
    "\n",
    "class Plotter:\n",
    "    def __init__(self):\n",
    "        self.f = plt.figure()\n",
    "        self.ax = plt.subplot(111)\n",
    "        self.ax.set_xlabel('$x$')\n",
    "        self.ax.set_ylabel('$u$')\n",
    "        \n",
    "    def update(self, mesh, time):\n",
    "        self.ax.plot(mesh.x[1:-1], mesh.u[1:-1], label=time)\n",
    "        self.ax.legend()\n",
    "                \n",
    "            \n",
    "def time_evolution(do_timestep, mesh, initial_time=0.0,\n",
    "                   number_steps=200, number_plots=10):\n",
    "    current_time = initial_time\n",
    "    mesh.plotter.update(mesh, current_time)\n",
    "    for n_step in range(number_steps):\n",
    "        do_timestep(mesh)\n",
    "        # swap u and u_tmp\n",
    "        u_swap = mesh.u\n",
    "        mesh.u = mesh.u_tmp\n",
    "        mesh.u_tmp = u_swap\n",
    "        current_time += mesh.deltat\n",
    "        if n_step % (number_steps//number_plots) == 0:\n",
    "            mesh.plotter.update(mesh, current_time)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. The python implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def timestep_python(mesh):\n",
    "    # boundaries\n",
    "    # main loop\n",
    "    for j in range(1, mesh.Nx + 1):\n",
    "        mesh.u_tmp[j] = mesh.u[j]\n",
    "\n",
    "mesh_py = Mesh(cfl=0.9)\n",
    "%time time_evolution(timestep_python, mesh_py)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Implementation using numpy arrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def timestep_numpy(mesh):\n",
    "    # boundaries\n",
    "    # main loop\n",
    "\n",
    "mesh_np = Mesh(cfl=0.9)\n",
    "%time time_evolution(timestep_numpy, mesh_np)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3a. Cython implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext Cython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cython -c=-O3 -c=-march=native -c=-ffast-math\n",
    "\n",
    "import numpy as np\n",
    "cimport numpy as np\n",
    "cimport cython\n",
    "\n",
    "ctypedef np.float64_t DTYPE_t\n",
    "\n",
    "def timestep_cython(mesh):\n",
    "    do_timestep_cython(mesh.u, mesh.u_tmp, mesh.alpha)\n",
    "\n",
    "@cython.boundscheck(False) # turn off bounds-checking for entire function\n",
    "@cython.wraparound(False)  # turn off negative index wrapping for entire function\n",
    "def do_timestep_cython(np.ndarray[DTYPE_t, ndim=1] u, np.ndarray[DTYPE_t, ndim=1] u_tmp,\n",
    "                       DTYPE_t alpha):\n",
    "    # boundaries\n",
    "    # main loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mesh_cy = Mesh(cfl=0.9)\n",
    "%time time_evolution(timestep_cython, mesh_cy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3b. Numba implementations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from numba import jit\n",
    "\n",
    "# ...\n",
    "def c_timestep_python_numba(u, u_tmp, alpha):\n",
    "    pass\n",
    "    # ...\n",
    "\n",
    "def timestep_python_numba(mesh):\n",
    "    \"\"\"Wrapper to pass only NumPy datatypes to the jitted function.\"\"\"\n",
    "    c_timestep_python_numba(mesh.u, mesh.u_tmp, mesh.alpha)\n",
    "        \n",
    "timestep_python_numba(Mesh(cfl=0.9, do_plot=False))  # call once to trigger compilation\n",
    "mesh_py_numba = Mesh(cfl=0.9)\n",
    "%time time_evolution(timestep_python_numba, mesh_py_numba)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from numba import jit\n",
    "\n",
    "# ...\n",
    "def c_timestep_numpy_numba(u, u_tmp, alpha):\n",
    "    pass\n",
    "    # ...\n",
    "\n",
    "def timestep_numpy_numba(mesh):\n",
    "    c_timestep_python_numba(mesh.u, mesh.u_tmp, mesh.alpha)\n",
    "        \n",
    "timestep_numpy_numba(Mesh(cfl=0.9, do_plot=False))  # call once to trigger compilation\n",
    "mesh_np_numba = Mesh(cfl=0.9)\n",
    "%time time_evolution(timestep_numpy_numba, mesh_np_numba)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Different ICs and CFL factors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Mesh2(Mesh):\n",
    "    def set_initial_conditions(self, type=1):\n",
    "        # implement something different here\n",
    "        if type==1:\n",
    "            self.u = np.zeros_like(self.x)\n",
    "            self.u[self.x <= 0.0] = 1.0\n",
    "        else:\n",
    "            raise ValueError(\"Type {:d} not known.\".format(type))\n",
    "\n",
    "mesh_np = Mesh2(cfl=0.9)\n",
    "%time time_evolution(timestep_numpy, mesh_np)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Extend to 2D\n",
    "* new Mesh and Plotter classes needed\n",
    "* new timestepper functions needed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
